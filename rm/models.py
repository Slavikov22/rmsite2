# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os.path
import shutil

from datetime import timedelta
from datetime import datetime

from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone
from django.http import Http404

from .managers import TrashManager, OperationManager

from smartrm2.trash import Trash as Smartrm2Trash
from smartrm2.config import Config
from smartrm2.rmerror import RMError
from smartrm2.rm import get_new_operation_id
from smartrm2.helpers.operation_helper import get_operation_files, delete_operation, restore_operation


class Trash(models.Model):
    path = models.CharField(max_length=255, unique=True)

    restore_policy = models.CharField(
        max_length=30, 
        choices=(
            ('passive', 'passive'),
            ('aggressive', 'aggressive'),
        ),
        default='passive',
    )

    maxtime = models.DurationField(default=timedelta(seconds=0))
    maxsize = models.BigIntegerField(default=0)
    maxnumber = models.BigIntegerField(default=0)

    create_time = models.DateTimeField(default=datetime.now)

    objects = TrashManager()

    def delete(self, *args, **kwargs):
        if os.path.exists(self.path):
            shutil.rmtree(self.path)

        super(Trash, self).delete(*args, **kwargs)

    def clear(self):
        self.get_smartrm2_trash().clear()

    def get_smartrm2_trash(self):
        config = Config(
            trashpath=self.path,
            restore_policy=self.restore_policy,
            autoclear_policies={'maxtime': self.maxtime.total_seconds()},
            maxsize=self.maxsize,
            maxnumber=self.maxnumber,
        )

        if not config.autoclear_policies['maxtime']:
            config.autoclear_policies = None

        return Smartrm2Trash(config)

    def __unicode__(self):
        return os.path.basename(self.path.rstrip('/'))


class Operation(models.Model):
    id = models.CharField(max_length=31, primary_key=True, unique=True,
                          default=get_new_operation_id())
    name = models.CharField(max_length=100, blank=True)
    trash = models.ForeignKey(Trash, on_delete=models.CASCADE)
    create_time = models.DateTimeField(default=datetime.now)

    objects = OperationManager()

    def restore_files(self, path, policy):
        smartrm2_trash = self.trash.get_smartrm2_trash()
        smartrm2_trash.config.restore_policy = policy
        
        try:
            restore_operation(int(self.id), smartrm2_trash, path)
        except RMError:
            pass

    def delete_files(self, path):
        smartrm2_trash = self.trash.get_smartrm2_trash()

        try:
            delete_operation(int(self.id), smartrm2_trash, path)
        except RMError:
            pass

    def delete(self, *args, **kwargs):
        smartrm2_trash = self.trash.get_smartrm2_trash()

        try:
            delete_operation(int(self.id), smartrm2_trash, '')
        except RMError:
            pass
            
        super(Operation, self).delete(*args, **kwargs)

    def get_files(self):
        return get_operation_files(
                   int(self.id), 
                   self.trash.get_smartrm2_trash()
               )

    def __unicode__(self):
        return self.name if self.name else self.id
