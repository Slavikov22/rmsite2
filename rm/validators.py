import os
import os.path

from django.core.exceptions import ValidationError

from .models import Trash, Operation


def trash_path_validator(path):
	if not os.path.isabs(path):
		raise ValidationError('Invalid path')

	if os.path.exists(path):
		if os.path.isfile(path):
			raise ValidationError('Invalid path')

		if not os.access(path, os.R_OK):
			raise ValidationError('Permission denied')
	else:
		if not os.path.exists(os.path.dirname(path)):
			raise ValidationError('Invalid path')

	for trash in Trash.objects.all():
		if path.startswith(trash.path) or trash.path.startswith(path):
			raise ValidationError('Intersecting directories')


def operation_paths_validator(paths):
	paths = [ path.strip() for path in paths.split(';') ]

	for path in paths:
		if path:
			if not os.path.isabs(path):
				raise ValidationError('Invalid path')

			if not os.path.exists(path):
				raise ValidationError('File is not exists')

			if not os.access(path, os.R_OK):
				raise ValidationError('Access denied')

			for trash in Trash.objects.all():
				if _is_paths_intersect(path, trash.path):
					raise ValidationError('Intersecting directories')

			intersect_count = 0
			for other_path in paths:
				if other_path:
					if _is_paths_intersect(path, other_path):
						intersect_count += 1

			if intersect_count > 1:
				raise ValidationError('Intersecting paths');

			
def operation_name_validator(name):
	for operation in Operation.objects.all():
		if name == operation.name:
			raise ValidationError('Name already used')


def abspaths_validator(paths):
	paths = [ path.strip() for path in paths.split(';') ]

	for path in paths:
		if path:
			if not os.path.isabs(path):
				raise ValidationError('Invalid path')


def _is_paths_intersect(path, other_path):
	if path == other_path:
		return True

	if len(path) > len(other_path):
		path, other_path = other_path, path

	return os.path.relpath(other_path, start=path).find('.') == -1;
