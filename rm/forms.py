from django import forms

from .models import Trash
from .validators import trash_path_validator, operation_paths_validator, operation_name_validator, abspaths_validator


class TrashForm(forms.Form):
	path = forms.CharField(max_length=100, validators=[trash_path_validator,])
	
	restore_policy = forms.ChoiceField(choices=(
		('passive', 'passive'),
		('aggressive', 'aggressive'),
	))
	
	maxtime_hours = forms.IntegerField(min_value=0, max_value=999, initial=0)
	maxtime_minutes = forms.IntegerField(min_value=0, max_value=59, initial=0)
	maxtime_seconds = forms.IntegerField(min_value=0, max_value=59, initial=0)
	maxsize = forms.IntegerField(min_value=0, max_value=10**12, initial=0)
	maxnumber = forms.IntegerField(min_value=0, max_value=10**6, initial=0)


class OperationForm(forms.Form):
	name = forms.CharField(max_length=30, required=False, 
		                   validators=[operation_name_validator,])

	paths = forms.CharField(widget=forms.Textarea, max_length=300,
		                    validators=[operation_paths_validator,])
	
	trash = forms.ModelChoiceField(queryset=Trash.objects.all())
	
	regex = forms.CharField(max_length=30, required=False)

	dry_run = forms.BooleanField(required=False)


class OperationRestoreForm(forms.Form):
	policy = forms.ChoiceField(choices=(
		('passive', 'passive'),
		('aggressive', 'aggressive'),
	))

	paths = forms.CharField(widget=forms.Textarea, max_length=300,
							validators=[abspaths_validator,])


class OperationDeleteForm(forms.Form):
	paths = forms.CharField(widget=forms.Textarea, max_length=300,
							validators=[abspaths_validator])
