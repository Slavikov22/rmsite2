# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os.path

from django.db import models

from smartrm2.rm import remove_file, remove_tree, get_new_operation_id, _is_equal_regex
from smartrm2.trash import Trash as Smartrm2Trash
from smartrm2.config import Config
from smartrm2.rmerror import RMError
from smartrm2.helpers.file_system_helper import get_correct_subpaths

class TrashManager(models.Manager):
    def create_trash(self, path, maxtime, maxsize, maxnumber):
        config = Config(
            trashpath=path,
            restore_policy='passive',
            autoclear_policies={'maxtime': maxtime.total_seconds()},
            maxsize=maxsize,
            maxnumber=maxnumber,
        )

        if not config.autoclear_policies['maxtime']:
            config.autoclear_policies = None

        Smartrm2Trash(config)

        trash = self.create(
            path=path,
            restore_policy=restore_policy,
            maxtime=maxtime,
            maxsize=maxsize,
            maxnumber=maxnumber,
        )

        return trash

    def get_queryset(self):
        queryset = super(TrashManager, self).get_queryset()
        for trash in queryset.all():
            try:
                if not os.path.exists(trash.path):
                    raise RMError('Trash not found')

                trash.get_smartrm2_trash()
            except RMError:
                trash.delete()

        return queryset


class OperationManager(models.Manager):
    def create_operation(self, paths, name, trash, regex, dry_run):
        operation_id = get_new_operation_id()
        smartrm2_trash = trash.get_smartrm2_trash()

        regex = regex if regex else '.*'

        paths = [ path.strip() for path in paths.split(';') ]
        for path in paths:
            if os.path.isfile(path):
                remove_file(
                    path=path,
                    trash=smartrm2_trash,
                    operation_id=operation_id,
                    dry_run=dry_run,
                )
            elif os.path.isdir(path):
                remove_tree(
                    path=path,
                    regex=regex,
                    trash=smartrm2_trash,
                    operation_id=operation_id,
                    dry_run=dry_run,
                )

        operation = self.create(
            id=str(operation_id),
            name=name,
            trash=trash,
        )

        if dry_run:
            correct_subpaths = []
            for path in paths:
                try:
                    correct_subpaths.extend(get_correct_subpaths(path))
                except RMError:
                    pass

            if regex:
                correct_subpaths = [ subpath for subpath in correct_subpaths
                                     if _is_equal_regex(subpath, regex)]

            return correct_subpaths

        return operation

    def get_queryset(self):
        queryset = super(OperationManager, self).get_queryset()

        for operation in queryset.all():
            try:
                operation.get_files()
            except RMError:
                operation.delete()

        return queryset
