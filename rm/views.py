# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_http_methods

from datetime import timedelta

from .models import Trash, Operation
from .forms import TrashForm, OperationForm, OperationRestoreForm, OperationDeleteForm

from smartrm2.rmerror import RMError


def index_view(request):
    return render(request, 'rm/index.html', {})


def trash_list_view(request):
    return render(
               request, 
               'rm/trashes.html', 
               {'trashes': Trash.objects.all()}
           )


def trash_detail_view(request, pk):
    trash = get_object_or_404(Trash, pk=pk)
    return render(request, 'rm/trash_detail.html', {'trash': trash})


def trash_create_view(request):
    if request.method == 'POST':
        form = TrashForm(request.POST)
        if form.is_valid():
            try:
                trash = Trash.objects.create_trash(
                    path=form.cleaned_data['path'], 
                    restore_policy=form.cleaned_data['restore_policy'],
                    maxtime=timedelta(
                        seconds=form.cleaned_data['maxtime_hours']*60*60 +\
                                form.cleaned_data['maxtime_minutes']*60 +\
                                form.cleaned_data['maxtime_seconds']
                    ),
                    maxsize=form.cleaned_data['maxsize'],
                    maxnumber=form.cleaned_data['maxnumber'],
                )
            except RMError as exc:
                return render(
                           request, 
                           'rm/error.html', 
                           {'message': exc.message}
                       )

            return HttpResponseRedirect(reverse('rm:trashes'))

    form = TrashForm()
    return render(request, 'rm/trash_create.html', {'form': form})


@require_http_methods(["POST",])
def trash_delete(request):
    trash = Trash.objects.get(pk=request.POST['pk'])
    trash.delete()
    return HttpResponseRedirect(reverse('rm:trashes'))


@require_http_methods(["POST",])
def trash_clear(request):
    trash = Trash.objects.get(pk=request.POST['pk'])
    trash.clear()
    return HttpResponseRedirect(reverse('rm:trashes'))


def operation_list_view(request):
    return render(
               request, 
               'rm/operations.html', 
               {'operations': Operation.objects.all()}
           )


def operation_detail_view(request, pk):
    operation = get_object_or_404(Operation, pk=pk)
    return render(
               request, 
               'rm/operation_detail.html', 
               {'operation': operation}
           )


def operation_create_view(request):
    if request.method == 'POST':
        form = OperationForm(request.POST)
        if form.is_valid():
            try:
                if form.cleaned_data['dry_run']:
                    files = Operation.objects.create_operation(
                        paths=form.cleaned_data['paths'],
                        name=form.cleaned_data['name'],
                        trash=form.cleaned_data['trash'],
                        regex=form.cleaned_data['regex'],
                        dry_run=form.cleaned_data['dry_run']
                    )

                    return render(
                               request, 
                               'rm/operation_create_dry_run.html', 
                               {'files': files}
                           )

                Operation.objects.create_operation(
                    paths=form.cleaned_data['paths'],
                    name=form.cleaned_data['name'],
                    trash=form.cleaned_data['trash'],
                    regex=form.cleaned_data['regex'],
                    dry_run=form.cleaned_data['dry_run']
                )
                return HttpResponseRedirect(reverse('rm:operations')) 
            except RMError as exc:
                return render(
                           request, 
                           'rm/error.html', 
                           {'message': exc.message}
                       )

    form = OperationForm()
    return render(request, 'rm/operation_create.html', {'form': form})


def operation_delete_view(request, pk):
    if request.method == 'POST':
        form = OperationDeleteForm(request.POST)
        if form.is_valid():
            operation = get_object_or_404(Operation, pk=pk)
            paths = [ path.strip() 
                      for path in form.cleaned_data['paths'].split(';')]

            for path in paths:
                if path:
                    operation.delete_files(path)
            return HttpResponseRedirect(reverse('rm:operations'))

    operation = get_object_or_404(Operation, pk=pk)
    form = OperationDeleteForm()
    return render(
               request, 
               'rm/operation_delete.html', 
               {'form': form, 'operation': operation}
           )


def operation_restore_view(request, pk):
    if request.method == 'POST':
        form = OperationRestoreForm(request.POST)
        if form.is_valid():
            operation = get_object_or_404(Operation, pk=pk)
            paths = [ path.strip() 
                      for path in form.cleaned_data['paths'].split(';')]

            for path in paths:
                if path:
                    operation.restore_files(
                        path, 
                        form.cleaned_data['policy']
                    )
            return HttpResponseRedirect(reverse('rm:operations'))

    operation = get_object_or_404(Operation, pk=pk)
    form = OperationRestoreForm()
    return render(
               request, 
               'rm/operation_restore.html', 
               {'form': form, 'operation': operation}
           )
